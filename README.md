# gogol

A simple Game of Life library.

## Installation

First, make sure you have initialized the gomodules in your project:

```shell
go mod init
```

Then download the package

```shell
go get gitlab.com/sitilge/gogol
```

## Usage

This is a simple sample:

```go
package main

import (
	"fmt"
	"gitlab.com/sitilge/gogol"
)

func main() {
	grid := [][]int{
		{0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 1, 1, 1, 0, 0, 0},
		{0, 0, 0, 1, 0, 1, 0, 0, 0},
		{0, 0, 0, 1, 1, 1, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0},
	}

	for i := 0; i < 8; i++ {
		fmt.Printf("Generation: %d\n", i)

		gogol.Print(grid)

		grid = gogol.Generate(grid)
	}
}
```
