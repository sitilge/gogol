package gogol

import "fmt"

//Generate generates and returns the next generation of the given grid.
func Generate(grid [][]int) [][]int {
	size := len(grid)

	next := make([][]int, size)
	for i := 0; i < size; i++ {
		next[i] = make([]int, size)
	}

	for i := 0; i < size; i++ {
		for j := 0; j < size; j++ {
			count := Neighbours(grid, i, j)
			switch {
			case count < 2 || count > 3:
				next[i][j] = 0
			case count == 2 && grid[i][j] == 1:
				next[i][j] = 1
			case count == 3:
				next[i][j] = 1
			}
		}
	}

	return next
}

func get(grid [][]int, i int, j int) int {
	size := len(grid)

	if (0 <= i && i < size) && (0 <= j && j < size) {
		if grid[i][j] == 1 {
			return 1
		}
	}

	return 0
}

//Neighbours returns the number of neighbours for the cell
//at the respective i,j coordinates in the given grid.
func Neighbours(grid [][]int, i int, j int) int {
	return get(grid, i-1, j-1) +
		get(grid, i-1, j) +
		get(grid, i-1, j+1) +
		get(grid, i, j-1) +
		get(grid, i, j+1) +
		get(grid, i+1, j-1) +
		get(grid, i+1, j) +
		get(grid, i+1, j+1)
}

//Print pretty prints the given grid.
func Print(grid [][]int) {
	size := len(grid)

	for i := 0; i < size; i++ {
		for j := 0; j < size; j++ {
			if grid[i][j] != 0 {
				fmt.Print("*")

				continue
			}

			fmt.Print(" ")
		}
		fmt.Println()
	}
}
